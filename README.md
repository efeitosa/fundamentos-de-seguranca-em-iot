# Fundamentos de Segurança em IoT



## Visão Geral

No mundo atual em que vivemos, onde mais dispositivos estão conectados e onde prevalece a Internet das Coisas (IoT), podendo ter suas vulnerabilidades, é importante que estudantes compreendam aspectos voltados à segurança destas redes, conhecendo seus fundamentos e maneiras de evitar invasões, resguardando os dados, compreendendo as tecnologias envolvidas.

## Ementa

1. Fundamentos de IoT;
2. Vulnerabilidades, ataques e contramedidas em IoT; 
3. Preservação da Privacidade em IoT;
4. Confiança e Autenticação em IoT;
5. Segurança de dados em IoT.

## Aulas

- [04/05] [Cenário de Segurança](https://gitlab.com/efeitosa/fundamentos-de-seguranca-em-iot/-/blob/main/Aulas/Cenario_Seguranca_FSIoT.pptx.pdf) 
- [04/05] [Conceitos de Segurança](https://gitlab.com/efeitosa/fundamentos-de-seguranca-em-iot/-/blob/main/Aulas/Conceitos_de_Seguranc%CC%A7a.pptx__1_.pdf) 
- [11/05] [IoT](https://gitlab.com/efeitosa/fundamentos-de-seguranca-em-iot/-/blob/main/Aulas/IoT.pdf) 
- [11/05] [Ameaças e Ataques](https://gitlab.com/efeitosa/fundamentos-de-seguranca-em-iot/-/blob/main/Aulas/Ameac%CC%A7as_e_Ataques.pdf) 
- [24/05] [Man in the Middle](https://gitlab.com/efeitosa/fundamentos-de-seguranca-em-iot/-/blob/main/Aulas/mitm.pdf) 
- [24/05] [OWASP IoT Top 10](https://gitlab.com/efeitosa/fundamentos-de-seguranca-em-iot/-/blob/main/Aulas/OWASP_IoT.pdf) 
- [08/06] [Análise de Firmware](https://gitlab.com/efeitosa/fundamentos-de-seguranca-em-iot/-/blob/main/Aulas/Analise_Firmware.pdf) 
- [15/06] **Analisando mais profundamente Firmwares** 
~~~
Usando os arquivos de firmware (os disponibilizados aqui e disponíveis na Internet), que apresentem file system, vamos usar o comando do linux dd e o binwalk para montar e extrair o firmware, e depois investigá-los até encontrar informações valiosas. Seu texto deve conter informações de pelo menos 2 relatórios, **PASSO A PASSO**: 
1. Execute o binwalk (exemplo: **binwalk DAP1353B-firmware-v316-rc015.bin**) para descobrir a localização do file system. A saída conterá essa informação.

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
112           0x70            Unix path: /dev/mtdblock/1
160           0xA0            LZMA compressed data, properties: 0x5D, dictionary size: 8388608 bytes, uncompressed size: 
852128        0xD00A0         PackImg section delimiter tag, little endian size: 14694912 bytes; big endian size: 3858432 bytes
852160        0xD00C0         Squashfs filesystem, big endian, version 3.0, size: 3855009 bytes, 793 inodes, blocksize: 65536 

2. use o comando dd para gerar um novo arquivo que contém apenas o file_system (dd if="firmware_em_análise" skip=xxxx bs=1 of="nome_do_arquivo_a_ser_analisado")
exemplo: **dd if=DAP1353B-firmware-v316-rc015.bin skip=852160 bs=1 of=filesystem_DAP1353B**

3. Execute o binwalk, com o parâmetro -e, para extrair o arquivo gerado (no caso do exemplo - filesystem_DAP1353B). O binwalk criará um diretório com o nome do arquivo, mas iniciando com _ (ex: _filesystem_DAP1353B)

4. Agora é só acessar o diretório e procurar as informações de usuário/senha e telnet
~~~
- [22/06] [Device Fingerprinting] (https://gitlab.com/efeitosa/fundamentos-de-seguranca-em-iot/-/blob/main/Aulas/Device_Fingerprinting.pdf) 
- [29/06] **Instalação e uso de Emulador de Firmware**
~~~
Nesta aula, faremos a instalação e configuração do EMUX, disponível em https://github.com/therealsaumil/emux.
No site do emux, além do processo de instalação e uso, também ficam disponíveis exemplos de como emular um firmware.

O objetivo da aula é realizar o processo de emulação de pelo menos um firmwares já existentes na 
ferramenta. Em outras palavras, seguir o passo a passo de instalação e uso do emux até a exibição da interface (página web) de um firmware.

DICAS
1. O emux é instalado via docker. Embora funcione no Windows, os comandos de instalação do emux foram feitos para Linux. Por isso é mais fácil instalar um docker dentro do Kali (se você não tiver uma máquina que execute Linux)
2. O site https://www.kali.org/docs/containers/installing-docker-on-kali/ apresenta o passo a passo para instalar o docker no Kali.
~~~

## Atividades

1. **Cenário de Segurança (04/05 - Entrega: 10/05)** 
~~~
Elabore um texto, de no máximo 2 páginas, que descreva sua visão sobre o atual cenário de segurança da informação. 
Seu texto deve conter informações de pelo menos 2 relatórios, podendo ser usados os listados (no repositório Relatórios), 
mas a busca por outros na internet é o mais recomendado. 
O uso de figuras é permitido, mas não exagere. O trabalho a ser entregue precisa ter sua visão de segurança e 
não uma monte de figuras. Envio para o email do professor (efeitosa@icomp.ufam.edu.br). Trabalho individual!
~~~

2. **Identificando Ataques em IoT (11/05 - Entrega: 17/05)** 
~~~
Análise um dos três datasets que contém tráfego IoT e ataques, disponibilizados pelo Hacking and Countermeasure Research Lab, no endereço https://ocslab.hksecurity.net/Datasets/iot-environment-dataset, utilizando a ferramenta Wireshark e apresente uma ou mais figuras que demonstrem a presença do ataque. 
Os datasets são: Port Scan Attack, OS & Service Detection Attack e HTTP Flooding Attack. Escolha apenas um dos três.
O trabalho deve ser enviado para o email do professor (efeitosa@icomp.ufam.edu.br). Trabalho individual!
~~~

3. **Descobrindo dispositivos IoT vulneráveis (24/05 - Entrega: 31/05)** 
~~~
O objetivo é identificar pelo menos 10 dispositivos IoT, acessíveis publicamente na Internet. Para tanto, utilize serviços públicos como shodan.io, censys, entre outros.
Após identificar seus "alvos", procure por vulnerabilidades relativas a cada serviço descoberto em cada alvo. Também tente identiticar quais vulnerabilidades do OWASP IoT Top 10 eles possuem.
Resumindo: monte um relatório completo sobre cada alvo.
O trabalho deve ser enviado para o email do professor (efeitosa@icomp.ufam.edu.br). Trabalho individual!
~~~

4. **Firmwares ainda permitem um MIRAI? (15/06 - Entrega: 22/06)** 
~~~
Como base na aula sobre Análise de Firmware e a prática de hoje (15/06), objetivo é identificar nos _file systems_ montados de pelo menos 5 firmwares (sendo pelo menos 1 diferente dos disponibilizados no gitlab) vestígios de: 
(1) telnet (o serviço telnet existe no filesystem? pode ser ativado?);
(2) login e senha (/etc/passwd e /etc/shadow); 
(3) servidor web (tem alguma configuração de acesso em código?)

Resumindo: monte um relatório descrevendo cada um dos 5 firmwares usados e seus achados.
O trabalho deve ser enviado para o email do professor (efeitosa@icomp.ufam.edu.br), incluindo firmware "novo". Trabalho individual!
~~~

5. **SSL Stripping e Arpspoof (22/06 - Entrega: 29/06)** [SSL Stripping e Arpspoof] (https://gitlab.com/efeitosa/fundamentos-de-seguranca-em-iot/-/blob/main/Aulas/SSLStripping_Arpspoof.pdf) 

6. **Emulando Firmwares (29/06 - Entrega: 06/07)** 
~~~
Como base na aula sobre emulação de Firmware, realizada hoje (29/06), elabore um relatório apresentando o processo de instalação e depois de emulação de dois firmwares não existentes na ferramenta emux. No relatório, mostre telas, comandos e até mesmo erros e falhas encontradas. 
O relatório deve estar em PDF e ser enviado para o email do professor (efeitosa@icomp.ufam.edu.br). Junto com o relatório, 
envie os dois firmwares não existentes no EMUX.

DICA
1. No github do emux (https://github.com/therealsaumil/emux/?tab=readme-ov-file), dentro da pasta docs, existe o tutorial "Emulating the DLINK DCS-935L IP Camera with EMUX". Embora esse firmware já exista hoje dentro do emux, esse tutorial explica o passo a passo de como adicionar um firmware no emux.
~~~

**TRABALHO FINAL**

[Analisando e Emulando Firmware IoT] (https://gitlab.com/efeitosa/fundamentos-de-seguranca-em-iot/-/blob/main/Aulas/AttifyOS.pdf) 

